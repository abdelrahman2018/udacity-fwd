/**
 * 
 * Manipulating the DOM exercise.
 * Exercise programmatically builds navigation,
 * scrolls to anchors from navigation,
 * and highlights section in viewport upon scrolling.
 * 
 * Dependencies: None
 * 
 * JS Version: ES2015/ES6
 * 
 * JS Standard: ESlint
 * 
*/

/**
 * Define Global Variables
 * 
*/


var nav_menu = document.querySelector(".navbar__menu");
var nav_list = document.querySelector("#navbar__list");
var sections = document.querySelectorAll("section");
var virtul_dom = new DocumentFragment();

/**
 * End Global Variables
 * Start Helper Functions
 * 
*/

function createAnchor(sec){
	let anchor = document.createElement("a");
		anchor.setAttribute("href", "#" + sec.getAttribute("id"));
		anchor.textContent = sec.getAttribute("data-nav");
		anchor.className = "menu__link";
		return anchor;
}



/**
 * End Helper Functions
 * Begin Main Functions
 * 
*/

// build the nav

function createNav(sections){
	console.log(nav_list);
	for(let sec of sections){
		let nav_item = document.createElement("li");
		let anchor = createAnchor(sec);
		nav_item.appendChild(anchor);
		virtul_dom.appendChild(nav_item);
	}

	nav_list.appendChild(virtul_dom);

}

function setActiveSection(sections){
	for(let sec of sections){
		const id = sec.getAttribute("id");
		const pos = sec.getBoundingClientRect();
		if (pos.top <= 100 && pos.bottom >= 100) {
			document.querySelector(`#${id}`).classList.add("active");
		}else{
			document.querySelector(`#${id}`).classList.remove("active");
		}
	}
}


// Add class 'active' to section when near top of viewport


// Scroll to anchor ID using scrollTO event


/**
 * End Main Functions
 * Begin Events
 * 
*/

window.addEventListener('DOMContentLoaded', (event) => {
    createNav(sections);
});

window.addEventListener("scroll", (event) => {
	console.log('hahah');
	setActiveSection(sections);
})

nav_menu.addEventListener("click", (event) => {
	let closest = event.target.closest(".menu__link");
	if (closest) {
		event.preventDefault();
		let clos_name = closest.getAttribute("href").slice(1, closest.length);
		let elm = document.getElementById(clos_name);
		elm.scrollIntoView({behavior : "smooth"});
	}
})

// Build menu 

// Scroll to section on link click

// Set sections as active

// createNav(sections);